package com.platzi;

import java.sql.Connection;
import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        // Press Alt+Intro with your caret at the highlighted text to see how
        // IntelliJ IDEA suggests fixing it.
        System.out.printf("Hello and welcome!\n");

        Scanner sc = new Scanner(System.in);

        int opcion = 0;

        do{
            System.out.println("----------------");
            System.out.println("Aplicacion de mensajes");
            System.out.println("1. crear mensajes");
            System.out.println("2. listar mensajes");
            System.out.println("3. editar mensajes");
            System.out.println("4. eliminar mensajes");
            System.out.println("5. salir");
            //leemos la opcion del user
            opcion = sc.nextInt();

            switch (opcion){
                case 1:
                    MensajesService.crearMensaje();
                    break;
                case 2:
                    MensajesService.listarMensajes();
                    break;
                case 3:
                    MensajesService.actualizarMensajes();
                    break;
                case 4:
                    MensajesService.borrarMensajes();
                    break;
                default:
                    break;
            }
        } while (opcion != 5);
    }
}