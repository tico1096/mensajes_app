package com.platzi;

import java.sql.*;

public class MensajesDAO {
    public static void crearMensajeDB(Mensajes mensaje) {
        String sql = "INSERT INTO mensajes (mensaje, autor_mensaje) VALUES (?, ?);";

        String value1 = mensaje.getMensaje();
        String value2 = mensaje.getAutorMensaje();

        ConnectDB connector = new ConnectDB();
        connector.insertData(sql, value1, value2);
        connector.closeConnection();
        System.out.println("Mensaje creado");
    }

    public static void leerMensajesDB() {
        String sql = "SELECT mensaje FROM mensajes";

        ConnectDB connector = new ConnectDB();
        ResultSet resultSet = connector.executeQuery(sql);

        try {
            while (resultSet.next()) {
                String value1 = resultSet.getString("mensaje");

                System.out.println("columna1: " + value1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connector.closeResultSet(resultSet);
            connector.closeConnection();
        }
    }

    public static void borrarMensajeDB(int idMensaje) {
        String sql = "DELETE FROM mensajes WHERE mensajes.id_mensaje = ?";
        int value1 = idMensaje;

        ConnectDB connector = new ConnectDB();
        connector.deleteData(sql, value1);
        connector.closeConnection();
        System.out.println("Mensaje eliminado");
    }

    public static void actualizarMensajeDB(Mensajes mensaje) {
        String sql = "UPDATE mensajes SET mensaje = ? WHERE id_mensaje = ?";
        String value1 = mensaje.getMensaje();
        int value2 = mensaje.getIdMensaje();

        ConnectDB connector = new ConnectDB();
        connector.updateData(sql, value1, value2);
        connector.closeConnection();
    }
}
