package com.platzi;

import java.util.Scanner;

public class MensajesService {

    public static void crearMensaje(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Escribe tu mensaje");
        String mensaje = sc.nextLine();

        System.out.println("tu nombre");
        String autor = sc.nextLine();

        Mensajes mensajeInsert = new Mensajes();
        mensajeInsert.setMensaje(mensaje);
        mensajeInsert.setAutorMensaje(autor);

        MensajesDAO.crearMensajeDB(mensajeInsert);

    }

    public static void listarMensajes(){
        MensajesDAO.leerMensajesDB();
    }

    public static void borrarMensajes(){
        Scanner sc = new Scanner(System.in);
        System.out.println("ID de mensaje");
        int idMensaje = sc.nextInt();

        Mensajes mensajeDelete = new Mensajes();
        mensajeDelete.setIdMensaje(idMensaje);

        MensajesDAO.borrarMensajeDB(mensajeDelete.getIdMensaje());

    }

    public static void actualizarMensajes(){
        Scanner sc = new Scanner(System.in);
        System.out.println("ID de mensaje");
        String idMensaje = sc.nextLine();

        System.out.println("mensaje a actualizar");
        String mensaje = sc.nextLine();

        Mensajes mensajeUpdate = new Mensajes();
        mensajeUpdate.setIdMensaje(Integer.parseInt(idMensaje));
        mensajeUpdate.setMensaje(mensaje);

        MensajesDAO.actualizarMensajeDB(mensajeUpdate);

    }
}
